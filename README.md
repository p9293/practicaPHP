# Práctica PHP

## General

En este examen se desarolla una solución web sencilla que contiene los mantenimientos de 3 tablas así como una petición por AJAX.

## Desarrollo

La solución se desarrolló con el Framework de Symfony 5.4 y el gestor de base de datos MySQL 8.0

## Requisitos

- Composer
- Servidor web
- MySQL 8.0
- PHP 7.4.9

En caso de algún error, instalar Symfony CLI (https://symfony.com/download)

## Despliegue

Si se descargan los archivos: 
  1. Descomprimir los archivos.
  2. Colocar la carpeta resultado en la carpeta del servidor web (www, htdocs o similares ).
  3. Entrar a la carpeta del proyecto, abrir en la terminar y colocar
        composer update
  4. Según el servidor web que utilice crear el virtualhost o similar.
  5. Ver el apartado ##Base de datos. 
  6. Entrar a la url que ha especificado. El proyecto debería de funcionar sin problemas.

Si se clona el repositorio: 
  1. Colocarse en la carpeta del servidor web (www, htdocs o similares ) y clonar el repositorio.
  2. Entrar a la carpeta del proyecto, abrir en la terminar y colocar
        composer update
  3. Según el servidor web que utilice crear el virtualhost o similar.
  4. Escoger el método que se desee utilizar para la base de datos. Ver el apartado ##Base de datos.
  5. Entrar a la url que ha especificado. El proyecto debería de funcionar sin problemas.

## Base de datos

Se creó un script inicial para la base de datos (Parte I del examen), pero luego se modificó un poco al
crearse las entidades en symfony. Por lo que hay dos archivos dentro de este proyecto **Parte 1** que se
refiere a la solución de la parte 1 y el archivo de **registro.sql** que contiene una base de datos con
registros para hacer pruebas. 

Para hacer la conexión con el proyecto y la base, debe de cambiar el archivo **.env** según su entorno. 
Importe la base utilizando el archivo de **registro.sql**.
