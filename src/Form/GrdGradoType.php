<?php

namespace App\Form;

use App\Entity\GrdGrado;
use App\Entity\MatMateria;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class GrdGradoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('grdNombre')
            ->add('mxg_id_mat', EntityType::class,[
                'required' => true,
                'class' => MatMateria::class,
                'choice_label'=>function (MatMateria $materia) {
                    return sprintf('%s | ',$materia->getMatNombre());
                }, 
                'multiple'=>true,
                'expanded'=>true, 
                'empty_data' => null]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GrdGrado::class,
        ]);
    }
}
