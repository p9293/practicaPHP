<?php

namespace App\Form;

use App\Entity\AlmAlumno;
use App\Entity\GrdGrado;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AlmAlumnoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('GrdId', EntityType::class, [
                'required' => true,
                'class' => GrdGrado::class,
                'choice_label' => function(GrdGrado $grado){
                    return sprintf('%s', $grado->getGrdNombre());
                },
                'placeholder' => 'Seleccione el grado...'
            ])
            ->add('almCodigo', null, ['empty_data' => null])
            ->add('almNombre', null, ['empty_data' => null])
            ->add('almEdad', null, ['empty_data' => null])
            ->add('almSexo', null, ['empty_data' => null])
            ->add('almObservacion', null, ['empty_data' => null])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AlmAlumno::class,
        ]);
    }
}
