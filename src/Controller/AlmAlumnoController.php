<?php

namespace App\Controller;

use App\Entity\AlmAlumno;
use App\Form\AlmAlumnoType;
use App\Repository\AlmAlumnosRepository;
use App\Repository\GrdGradoRepository;
use App\Repository\MatMateriaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse as HttpFoundationJsonResponse;
/**
 * @Route("/alm/alumno")
 */
class AlmAlumnoController extends AbstractController
{
    /**
     * @Route("/", name="app_alm_alumno_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $almAlumnos = $entityManager
            ->getRepository(AlmAlumno::class)
            ->findAll();

        return $this->render('alm_alumno/index.html.twig', [
            'alm_alumnos' => $almAlumnos,
        ]);
    }

    /**
     * @Route("/new", name="app_alm_alumno_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $almAlumno = new AlmAlumno();
        $form = $this->createForm(AlmAlumnoType::class, $almAlumno);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($almAlumno);
            $entityManager->flush();

            return $this->redirectToRoute('app_alm_alumno_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('alm_alumno/new.html.twig', [
            'alm_alumno' => $almAlumno,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{almId}", name="app_alm_alumno_show", methods={"GET"})
     */
    public function show(AlmAlumno $almAlumno): Response
    {
        return $this->render('alm_alumno/show.html.twig', [
            'alm_alumno' => $almAlumno,
        ]);
    }

    /**
     * @Route("/{almId}/edit", name="app_alm_alumno_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, AlmAlumno $almAlumno, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(AlmAlumnoType::class, $almAlumno);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_alm_alumno_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('alm_alumno/edit.html.twig', [
            'alm_alumno' => $almAlumno,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{almId}", name="app_alm_alumno_delete", methods={"POST"})
     */
    public function delete(Request $request, AlmAlumno $almAlumno, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$almAlumno->getAlmId(), $request->request->get('_token'))) {
            $entityManager->remove($almAlumno);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_alm_alumno_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/a/consulta", name="materia_alumno", methods={"GET","POST"})
    */
    public function materiasAlumno(Request $request, AlmAlumnosRepository $alm, MatMateriaRepository $m, GrdGradoRepository $grd)
    {
        $idAlumno = $request->get('idAlumno');
        $a = intval($idAlumno);
        
        if($a != 0){
            $alumno = $alm->findOneBy(array('almId'=> $a));
            $materias = $alumno->getGrdId()->getMxgIdMat();

            foreach ($materias as $value)
            {
                $nmat = $m->findOneBy(array('matId'=> $value)); 
                $nomMateria [] = $nmat->getMatNombre();
            }
            
            $response = array("code" => 200,"dv" => $this->render('inicio/infoAlumno.html.twig', 
                array('alm_alumno' => $alumno, 'materias' => $nomMateria))->getContent());
        }
        else{
            $alumnos = $alm->findAll();
            if($alumnos != [])
            {
                foreach($alumnos as $value)
                {
                    $nAlum = $value->getAlmNombre();
                    $nomMat = $value->getGrdId()->getGrdNombre();
                    $materias = $value->getGrdId()->getMxgIdMat();
                    
                    $nomMateria = [];
                    foreach ($materias as $value)
                    {
                        $nmat = $m->findOneBy(array('matId'=> $value)); 
                        $nomMateria [] = $nmat->getMatNombre();
                    }
                    
                    $data[] = [
                        $nAlum, $nomMat, $nomMateria
                    ];
                    
                } 
                $response = array("code" => 200,"dv" => $this->render('inicio/informacion.html.twig', 
                    array('alumnos'=> $data))->getContent());
            }
        }

        return new HttpFoundationJsonResponse($response); 
    }
}
