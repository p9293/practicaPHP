<?php

namespace App\Controller;

use App\Repository\AlmAlumnosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InicioController extends AbstractController
{
    /**
     * @Route("/", name="app_inicio")
     */
    public function index(AlmAlumnosRepository $alm): Response
    {
        $alumnos = $alm->findAll();
        return $this->render('inicio/index.html.twig', [
            'alm_alumnos' => $alumnos,
        ]);
    }
}
