<?php

namespace App\Controller;

use App\Entity\GrdGrado;
use App\Form\GrdGradoType;
use App\Repository\MatMateriaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/grd/grado")
 */
class GrdGradoController extends AbstractController
{
    /**
     * @Route("/", name="app_grd_grado_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $grdGrados = $entityManager
            ->getRepository(GrdGrado::class)
            ->findAll();

        return $this->render('grd_grado/index.html.twig', [
            'grd_grados' => $grdGrados,
        ]);
    }

    /**
     * @Route("/new", name="app_grd_grado_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $grdGrado = new GrdGrado();
        $form = $this->createForm(GrdGradoType::class, $grdGrado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($grdGrado);
            $entityManager->flush();

            return $this->redirectToRoute('app_grd_grado_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('grd_grado/new.html.twig', [
            'grd_grado' => $grdGrado,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{grdId}", name="app_grd_grado_show", methods={"GET"})
     */
    public function show(GrdGrado $grdGrado, MatMateriaRepository $mat): Response
    {
        $materias = $grdGrado->getMxgIdMat();

        foreach ($materias as $value)
        {
            $nmat = $mat->findOneBy(array('matId'=> $value)); 
            $nomMateria [] = $nmat->getMatNombre();
        }
        return $this->render('grd_grado/show.html.twig', [
            'grd_grado' => $grdGrado,
            'materias' => $nomMateria
        ]);
    }

    /**
     * @Route("/{grdId}/edit", name="app_grd_grado_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, GrdGrado $grdGrado, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(GrdGradoType::class, $grdGrado);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_grd_grado_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('grd_grado/edit.html.twig', [
            'grd_grado' => $grdGrado,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{grdId}", name="app_grd_grado_delete", methods={"POST"})
     */
    public function delete(Request $request, GrdGrado $grdGrado, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$grdGrado->getGrdId(), $request->request->get('_token'))) {
            $entityManager->remove($grdGrado);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_grd_grado_index', [], Response::HTTP_SEE_OTHER);
    }
}
