<?php

namespace App\Controller;

use App\Entity\MatMateria;
use App\Form\MatMateriaType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mat/materia")
 */
class MatMateriaController extends AbstractController
{
    /**
     * @Route("/", name="app_mat_materia_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $matMaterias = $entityManager
            ->getRepository(MatMateria::class)
            ->findAll();

        return $this->render('mat_materia/index.html.twig', [
            'mat_materias' => $matMaterias,
        ]);
    }

    /**
     * @Route("/new", name="app_mat_materia_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $matMaterium = new MatMateria();
        $form = $this->createForm(MatMateriaType::class, $matMaterium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($matMaterium);
            $entityManager->flush();

            return $this->redirectToRoute('app_mat_materia_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('mat_materia/new.html.twig', [
            'mat_materium' => $matMaterium,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{matId}", name="app_mat_materia_show", methods={"GET"})
     */
    public function show(MatMateria $matMaterium): Response
    {
        return $this->render('mat_materia/show.html.twig', [
            'mat_materium' => $matMaterium,
        ]);
    }

    /**
     * @Route("/{matId}/edit", name="app_mat_materia_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, MatMateria $matMaterium, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(MatMateriaType::class, $matMaterium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_mat_materia_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('mat_materia/edit.html.twig', [
            'mat_materium' => $matMaterium,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{matId}", name="app_mat_materia_delete", methods={"POST"})
     */
    public function delete(Request $request, MatMateria $matMaterium, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$matMaterium->getMatId(), $request->request->get('_token'))) {
            $entityManager->remove($matMaterium);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_mat_materia_index', [], Response::HTTP_SEE_OTHER);
    }
}
