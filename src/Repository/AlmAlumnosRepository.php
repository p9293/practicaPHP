<?php

namespace App\Repository;

use App\Entity\AlmAlumno;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AlmAlumno>
 *
 * @method AlmAlumno|null find($id, $lockMode = null, $lockVersion = null)
 * @method AlmAlumno|null findOneBy(array $criteria, array $orderBy = null)
 * @method AlmAlumno[]    findAll()
 * @method AlmAlumno[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlmAlumnosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AlmAlumno::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(AlmAlumno $entity, bool $flush = false): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(AlmAlumno $entity, bool $flush = false): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

}
