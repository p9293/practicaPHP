<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MatMateria
 *
 * @ORM\Table(name="mat_materia")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\MatMateriaRepository")
 */
class MatMateria
{
    /**
     * @var int
     *
     * @ORM\Column(name="MAT_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $matId;

    /**
     * @var string
     *
     * @ORM\Column(name="MAT_NOMBRE", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message="Ingrese el nombre de la materia.")
     */
    private $matNombre;

     /**
     * @var \Doctrine\Common\Collections\Collection
     * 
     * @ORM\ManyToMany(targetEntity="GrdGrado", mappedBy="mxg_id_mat")
     */
    private $mxg_id_grd;

    public function __construct()
    {
        $this->mxg_id_grd = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->getMatId();
    }

    public function getMatId(): ?int
    {
        return $this->matId;
    }

    public function getMatNombre(): ?string
    {
        return $this->matNombre;
    }

    public function setMatNombre(string $matNombre): self
    {
        $this->matNombre = $matNombre;

        return $this;
    }

    /**
     * @return Collection<int, GrdGrado>
     */
    public function getMxgIdGrd(): Collection
    {
        return $this->mxg_id_grd;
    }

    public function addMxgIdGrd(GrdGrado $mxgIdGrd): self
    {
        if (!$this->mxg_id_grd->contains($mxgIdGrd)) {
            $this->mxg_id_grd[] = $mxgIdGrd;
            $mxgIdGrd->addMxgIdMat($this);
        }

        return $this;
    }

    public function removeMxgIdGrd(GrdGrado $mxgIdGrd): self
    {
        if ($this->mxg_id_grd->removeElement($mxgIdGrd)) {
            $mxgIdGrd->removeMxgIdMat($this);
        }

        return $this;
    }

}
