<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * AlmAlumno
 *
 * @ORM\Table(name="alm_alumno")
 * @ORM\Entity
 * @UniqueEntity(fields="almCodigo", message="Este código ya fue asignado a otro alumno")
 * @ORM\Entity(repositoryClass="App\Repository\AlmAlumnosRepository")
 */
class AlmAlumno
{
    /**
     * @var int
     *
     * @ORM\Column(name="ALM_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $almId;

    /**
     * @var \App\Entity\GrdGrado
     *
     * @ORM\ManyToOne(targetEntity="GrdGrado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="grd_id", referencedColumnName="GRD_ID")
     * })
     */
    private $GrdId;

    /**
     * @var string
     *
     * @ORM\Column(name="ALM_CODIGO", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message="Ingrese el código del alumno.")
     */
    private $almCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="ALM_NOMBRE", type="string", length=300, nullable=false)
     * @Assert\NotBlank(message="Ingrese el nombre del alumno.")
     */
    private $almNombre;

    /**
     * @var int
     *
     * @ORM\Column(name="ALM_EDAD", type="integer", nullable=false)
     * @Assert\NotBlank(message="Ingrese la edad del alumno.")
     */
    private $almEdad;

    /**
     * @var string
     *
     * @ORM\Column(name="ALM_SEXO", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message="Ingrese el sexo del alumno.")
     */
    private $almSexo;

    /**
     * @var string
     *
     * @ORM\Column(name="ALM_OBSERVACION", type="string", length=300, nullable=true)
     */
    private $almObservacion;

    public function getAlmId(): ?int
    {
        return $this->almId;
    }

    public function getAlmCodigo(): ?string
    {
        return $this->almCodigo;
    }

    public function setAlmCodigo(string $almCodigo): self
    {
        $this->almCodigo = $almCodigo;

        return $this;
    }

    public function getAlmNombre(): ?string
    {
        return $this->almNombre;
    }

    public function setAlmNombre(string $almNombre): self
    {
        $this->almNombre = $almNombre;

        return $this;
    }

    public function getAlmEdad(): ?int
    {
        return $this->almEdad;
    }

    public function setAlmEdad(int $almEdad): self
    {
        $this->almEdad = $almEdad;

        return $this;
    }

    public function getAlmSexo(): ?string
    {
        return $this->almSexo;
    }

    public function setAlmSexo(string $almSexo): self
    {
        $this->almSexo = $almSexo;

        return $this;
    }

    public function getAlmObservacion(): ?string
    {
        return $this->almObservacion;
    }

    public function setAlmObservacion(string $almObservacion): self
    {
        $this->almObservacion = $almObservacion;

        return $this;
    }

    public function getGrdId(): ?GrdGrado
    {
        return $this->GrdId;
    }

    public function setGrdId(?GrdGrado $GrdId): self
    {
        $this->GrdId = $GrdId;

        return $this;
    }

}
