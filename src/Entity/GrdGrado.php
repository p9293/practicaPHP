<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * GrdGrado
 *
 * @ORM\Table(name="grd_grado")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\GrdGradoRepository")
 * 
 */
class GrdGrado
{
    /**
     * @var int
     *
     * @ORM\Column(name="GRD_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $grdId;

    /**
     * @var string
     *
     * @ORM\Column(name="GRD_NOMBRE", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message="Ingrese el nombre del grado.")
     */
    private $grdNombre;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * 
     * @ORM\ManyToMany(targetEntity="MatMateria", inversedBy="mxg_id_grd")
     *  @ORM\JoinTable(name="mxg_materiasxgrado",
     *   joinColumns={
     *     @ORM\JoinColumn(name="grd_id", referencedColumnName="GRD_ID")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="mat_id", referencedColumnName="MAT_ID")
     *   })
     */
    private $mxg_id_mat;

    public function __construct()
    {
        $this->mxg_id_mat = new ArrayCollection();
    }

    public function getGrdId(): ?int
    {
        return $this->grdId;
    }

    public function getGrdNombre(): ?string
    {
        return $this->grdNombre;
    }

    public function setGrdNombre(string $grdNombre): self
    {
        $this->grdNombre = $grdNombre;

        return $this;
    }

    /**
     * @return Collection|MatMateria[]
     */
    public function getMxgIdMat(): Collection
    {
        return $this->mxg_id_mat;
    }

    public function addMxgIdMat(MatMateria $mxgIdMat): self
    {
        if (!$this->mxg_id_mat->contains($mxgIdMat)) {
            $this->mxg_id_mat[] = $mxgIdMat;
        }

        return $this;
    }

    public function removeMxgIdMat(MatMateria $mxgIdMat): self
    {
        $this->mxg_id_mat->removeElement($mxgIdMat);

        return $this;
    }
}
