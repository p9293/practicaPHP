-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 10-05-2022 a las 21:14:25
-- Versión del servidor: 8.0.27
-- Versión de PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `registro`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alm_alumno`
--

DROP TABLE IF EXISTS `alm_alumno`;
CREATE TABLE IF NOT EXISTS `alm_alumno` (
  `grd_id` int DEFAULT NULL,
  `ALM_ID` int NOT NULL AUTO_INCREMENT,
  `ALM_CODIGO` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ALM_NOMBRE` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ALM_EDAD` int NOT NULL,
  `ALM_SEXO` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ALM_OBSERVACION` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ALM_ID`),
  KEY `IDX_5C7C2D32A655198` (`grd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `alm_alumno`
--

INSERT INTO `alm_alumno` (`grd_id`, `ALM_ID`, `ALM_CODIGO`, `ALM_NOMBRE`, `ALM_EDAD`, `ALM_SEXO`, `ALM_OBSERVACION`) VALUES
(1, 1, 'ALM-00001', 'Luis Medina', 7, 'Masculino', NULL),
(3, 2, 'ALM-00002', 'Rosa Esmeralda Torres', 9, 'Femenino', NULL),
(2, 3, 'ALM-00003', 'Raúl Morales', 8, 'Masculino', 'Buen alumno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grd_grado`
--

DROP TABLE IF EXISTS `grd_grado`;
CREATE TABLE IF NOT EXISTS `grd_grado` (
  `GRD_ID` int NOT NULL AUTO_INCREMENT,
  `GRD_NOMBRE` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`GRD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `grd_grado`
--

INSERT INTO `grd_grado` (`GRD_ID`, `GRD_NOMBRE`) VALUES
(1, 'Primer grado'),
(2, 'Segundo grado'),
(3, 'Tercer grado'),
(4, 'Cuarto grado'),
(5, 'Quinto grado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mat_materia`
--

DROP TABLE IF EXISTS `mat_materia`;
CREATE TABLE IF NOT EXISTS `mat_materia` (
  `MAT_ID` int NOT NULL AUTO_INCREMENT,
  `MAT_NOMBRE` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`MAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mat_materia`
--

INSERT INTO `mat_materia` (`MAT_ID`, `MAT_NOMBRE`) VALUES
(1, 'Ciencias sociales'),
(2, 'Matemática'),
(3, 'Inglés'),
(4, 'Lenguaje y literatura'),
(5, 'Biología');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `messenger_messages`
--

DROP TABLE IF EXISTS `messenger_messages`;
CREATE TABLE IF NOT EXISTS `messenger_messages` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `available_at` datetime NOT NULL,
  `delivered_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  KEY `IDX_75EA56E016BA31DB` (`delivered_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mxg_materiasxgrado`
--

DROP TABLE IF EXISTS `mxg_materiasxgrado`;
CREATE TABLE IF NOT EXISTS `mxg_materiasxgrado` (
  `grd_id` int NOT NULL,
  `mat_id` int NOT NULL,
  PRIMARY KEY (`grd_id`,`mat_id`),
  KEY `IDX_5A9A504CA655198` (`grd_id`),
  KEY `IDX_5A9A504CDCA7C833` (`mat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `mxg_materiasxgrado`
--

INSERT INTO `mxg_materiasxgrado` (`grd_id`, `mat_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 5),
(4, 2),
(5, 1),
(5, 2),
(5, 3);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alm_alumno`
--
ALTER TABLE `alm_alumno`
  ADD CONSTRAINT `FK_5C7C2D32A655198` FOREIGN KEY (`grd_id`) REFERENCES `grd_grado` (`GRD_ID`);

--
-- Filtros para la tabla `mxg_materiasxgrado`
--
ALTER TABLE `mxg_materiasxgrado`
  ADD CONSTRAINT `FK_5A9A504CA655198` FOREIGN KEY (`grd_id`) REFERENCES `grd_grado` (`GRD_ID`),
  ADD CONSTRAINT `FK_5A9A504CDCA7C833` FOREIGN KEY (`mat_id`) REFERENCES `mat_materia` (`MAT_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
